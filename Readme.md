# Simple Todos API

This is a simple *Todos* api to serve as a *server-sync* for an [Ionic Simple todo application](https://bitbucket.org/rapito/ionic-simple-todos).

*Naive implementation for proof-of-concept*

*Built with meteor: [Install](https://www.meteor.com/install)*

## Working demo

- [Demo](http://ionic-todos-api.meteor.com/api/lists/)
 
## How to run

- cd into project root
- ``` meteor ```
- Test it with the ionic app: [ionic-simple-todo](https://bitbucket.org/rapito/ionic-simple-todos)
- Or in your browser: [localhost](http://localhost:3000/api/fields)

## Dependencies

- [Restivus](https://github.com/kahmali/meteor-restivus/)