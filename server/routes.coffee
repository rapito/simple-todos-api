# Global API configuration
Api = new Restivus
  authRequired: false
  prettyJson: true

Api.addCollection Lists
Api.addCollection Apps

Api.addRoute 'lists/save',
  authRequired: false
  post: ->
#    console.log this
    lists = this.request.body
    Lists.remove({});
    for l in lists
      if l?.name and l?.tasks
        Lists.insert {name: l.name, tasks: l.tasks}

    return {
    statusCode: 200
    headers:
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Method': '*',
      'Access-Control-Allow-Headers': 'Content-Type, X-User-Id, X-Auth-Token'
    }